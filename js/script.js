$(document).ready(function () {
	$('.fa-check-square').on('click', allChecked);
	$('.fa-check-square').on('dblclick', allReChecked);
	$('#times-circle').on('click', DelTask);
	$('#inpSubm').data('counter', 100).on('click', inpSubm);
	$('.inpText').data('counter', 0).keydown(function (e) {
		if (e.keyCode === 13) {
			var value1 = $.trim($('.inpText').val());
			if (value1.length > 0) {
				var value = $('.inpText').val();
				$('.inpText').val('');
				var counter = $('.inpText').data('counter'); // Получаем значение
				$('.inpText').data('counter', counter + 1); // Увеличиваем значение на 1
				$('.tbody').append('<tr class="tr' + counter + '" style="display: none;"><th scope="row"><input type="checkbox" class="form-check-input" id="exampleCheck1"></th><td id="' + counter + '" class="td" title="Двойной клик отменит выполнение">' + value + '</td><td></td></tr>');
				var tra = ".tr" + counter + "";
				$(tra).show(500);
			} else {
				alert('Введите название Таски');
			}
		}
	});
	var cachelog = localStorage['myKey'];
	if (undefined !== cachelog && cachelog.length >= 1) {
		var counter = $('.inpText').data('counter'); // Получаем значение
		$('.inpText').data('counter', counter + 1);
		$('.tbody').append(cachelog);
	}
	window.onbeforeunload = closingCode;

	function closingCode() {
		localStorage['myKey'] = $('.tbody').html();
		return null;
	}

	function reloadDone() {
		$('.td').one('click', doneTask);
		$('.td').one('dblclick', doneTaskOff);
	}
	var reloadDone1 = setInterval(reloadDone, 1000);
});

function date() {
	document.cookie = "userName=Vasya";
	alert(document.cookie);
}


function allChecked() {
	$('input[type="checkbox"]').attr("checked", "checked");
}

function allReChecked() {
	$('input[type="checkbox"]').removeAttr("checked", "checked");
}

function DelTask() {
	var counter1 = 0;
	var $input1 = $('input[type="checkbox"]');
	if ($input1.is(":checked") == true) {
		while (counter1 < 200) {
			var $input = $('.tr' + counter1 + '>th>input[type="checkbox"]');
			if ($input.is(":checked") == true) {
				$('.tr' + counter1 + '').animate({
					opacity: 0
				}, 600, function () {
					$(this).remove()
				});
			}
			counter1++;
		}
	} else {
		alert('Выбирите хотя бы одну таску');
	}
}

function inpSubm() {

	var value1 = $.trim($('.inpText').val());
	if (value1.length > 0) {
		var value = $('.inpText').val();
		$('.inpText').val('');
		var counter1 = $('.inpSubm').data('counter'); // Получаем значение
		$(this).data('counter', counter + 1); // Увеличиваем значение на 1
		var counter = $('.inpText').data('counter'); // Получаем значение
		$('.inpText').data('counter', counter + 1); // Увеличиваем значение на 1
		$('.tbody').append('<tr class="tr' + counter + '" style="display: none;"><th scope="row"><input type="checkbox" class="form-check-input" id="exampleCheck1"></th><td id="' + counter + '" class="td" title="Двойной клик отменит выполнение">' + value + '</td><td></td></tr>');
		var tra = ".tr" + counter + "";
		$(tra).show(500);
	} else {
		alert('Введите название Таски')
	}
};

function doneTask() {
		$(this).addClass('doneTask');
}

function doneTaskOff() {
		$(this).removeClass('doneTask');
}
